#ifndef SOUND_IOCTL_H
#define SOUND_IOCTL_H

///// Wrappers to OSS ioctl() calls /////

int sound_set_format(int fd, int format);
int sound_set_srate(int fd, int srate);
int sound_set_channels(int fd, int channels);

#endif
