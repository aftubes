#ifndef SOUNDOUT_H
#define SOUNDOUT_H

#include "aformat.h"
#include "stddef.h"

struct soundout {
    int fd;
};

int soundout_open(struct soundout *so, const struct aformat *af);
int soundout_write(struct soundout *so, const void *buf, size_t len);

#endif
