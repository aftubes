#ifndef WAVEFILE_H
#define WAVEFILE_H

#include "aformat.h"
#include "stdio.h"
#include "sys/types.h"
#include "errors.h"

enum {
    EFOPEN = ERR_GROUP(8),  // fopen failed
    EFREAD,                 // fread failed
    EFORMAT,                // invalid file format
    EWAVE_EOF,              // read beyond the end of the wave file
};

struct waveformat {
    int srate;
    int bits;
    int channels;
};

struct wavefile
{
    const char *filename;
    FILE *f;
    struct waveformat format;
    long length;    // length, in samples
    off_t pcm_start; // file offset to start of actual audio data
};

err_t wavefile_open(struct wavefile *wav, const char *filename);
err_t wavefile_read_at(struct wavefile *wav, off_t sample_start, void *buf, size_t n_samples);

#endif
