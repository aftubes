#include "buffer.h"
#include "stdlib.h"

void buffer_init(struct buffer *buf)
{
    buf->n_samples = 0;
    buf->data = NULL;
}

err_t buffer_alloc(struct buffer *buf, size_t n_samples)
{
    void *new_buf;
    if (buf->n_samples != n_samples){
        new_buf = realloc(buf->data, n_samples * aformat_get_sample_size(&buf->format));
        if (!new_buf){
            return make_error(ENOMEM, NULL, "memory full: allocating buffer (%d samples)", n_samples);
        }
        buf->data = new_buf;
        buf->n_samples = n_samples;
    }
    return EOK;
}
