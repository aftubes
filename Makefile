.POSIX:
.SILENT:

## TOOLS ##

CC := gcc
LD := gcc
MAKEDEPEND := makedepend

## OPTIONS ##

BUILDDIR := build/
CPPDEFINE := _GNU_SOURCE _POSIX_SOURCE
CPPPATH := .
CFLAGS := -Wall -Werror -std=c99 -pedantic -O3 -g -ffunction-sections
LDFLAGS := -Wl,--gc-sections

## SOURCES ##

progname := aftubes

subdirs := modules/

sources := aformat.c buffer.c errors.c graph.c main.c sound-ioctl.c soundout.c \
	wavefile.c \
	$(addprefix modules/,convert.c mixer.c playsink.c ringmod.c wavesource.c)

## RULES ##

hackydir := `gcc -print-search-dirs | grep install | cut -d" " -f2`/include
CPPFLAGS := $(addprefix -I,$(CPPPATH)) $(addprefix -D,$(CPPDEFINE))
CFLAGS := $(CPPFLAGS) $(CFLAGS)
objects := $(addprefix $(BUILDDIR), $(sources:.c=.o))

green := \033[32m
ungreen := \033[0m

.PHONY: all
all: makedirs $(BUILDDIR)$(progname)

.PHONY: makedirs
makedirs:
	mkdir -p $(addprefix $(BUILDDIR),$(subdirs))

$(BUILDDIR)$(progname): $(objects)
	printf "         Linking $(green)$@$(ungreen)\n"
	$(LD) $(LDFLAGS) -o $@ $(objects)

ifneq ($(MAKECMDGOALS),clean)
-include .depends.mak
endif

.depends.mak:
	printf "        Scanning $(green)all source files$(ungreen)\n"
	$(MAKEDEPEND) -I/usr/ -I$(hackydir) $(CPPFLAGS) -f- -p"$@ $(BUILDDIR)" $(sources) > $@

$(BUILDDIR)%.o: %.c
	printf "       Compiling $(green)$@$(ungreen)\n"
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: clean
clean:
	printf "        Deleting $(green)all objects$(ungreen)\n"
	$(RM) -r $(BUILDDIR)
