#include "wavefile.h"
#include "stdint.h"
#include "assert.h"
#include "string.h"
#include "attr.h"
#include "errors.h"

struct wavehdr
{
    // http://mathmatrix.narod.ru/Wavefmt.html
    char magic[4];      // "RIFF"
    uint32_t size;      // size of file
    char wave_magic[4]; // "WAVE"
    char fmt_magic[4];  // "fmt "
    uint32_t wave_section_chunk_size;
    uint16_t wave_fmt_type; // 1 = linear quantization
    uint16_t n_channels;
    uint32_t srate;
    uint32_t bytes_per_second;
    uint16_t block_align;
    uint16_t bits_per_sample;
};

struct chunkhdr
{
    char name[4];   // name of chunk
    uint32_t size;  // size (bytes) of chunk (excluding header)
};

pure int waveformat_get_sample_size(const struct waveformat *restrict wf)
{
    return (wf->bits / 8) * wf->channels;
}

static err_t read_header(struct wavefile *wav)
{
    struct wavehdr whdr;
    struct chunkhdr chdr;

    // read wave header
    if (fread(&whdr, sizeof whdr, 1, wav->f) < 1){
        return make_error(EFREAD, wav, "%s: could not read wave file header", wav->filename);
    }
    
    // check wave magic strings
    if (strncmp(whdr.magic, "RIFF", 4)
      || strncmp(whdr.wave_magic, "WAVE", 4)
      || strncmp(whdr.fmt_magic, "fmt ", 4)){
        // invalid file format
        return make_error(EFORMAT, wav, "%s: invalid wave file format", wav->filename);
    }

    // make sure it's in PCM format
    if (whdr.wave_fmt_type != 1){
        return make_error(EFORMAT, wav, "%s: unsupported wave file format", wav->filename);
    }

    // store format information
    wav->format.srate = whdr.srate;
    wav->format.bits = whdr.bits_per_sample;
    wav->format.channels = whdr.n_channels;

    // read chunk headers
    for (;;){
        if (fread(&chdr, sizeof chdr, 1, wav->f) < 1){
            // read failure
            return 1;
        }
        if (!strncmp(chdr.name, "data", 4)){
            // found the data chunk
            break;
        } else {
            // this is not the data.
            // Next chunk!
            fseek(wav->f, chdr.size, SEEK_CUR);
        }
    }

    wav->length = chdr.size / ((wav->format.bits / 8) * wav->format.channels);
    wav->pcm_start = ftell(wav->f);
    return 0;
}

err_t wavefile_open(struct wavefile *wav, const char *filename)
{
    // open the wave file
    wav->filename = filename; // TODO: dup?
    wav->f = fopen(filename, "r");
    if (!wav->f){
        return make_error(EFOPEN, wav, "%s: cannot open file", filename);
    }

    // read wave file header
    return read_header(wav);
}

err_t wavefile_read_at(struct wavefile *wav, off_t sample_start, void *buf, size_t n_samples)
{
    if (sample_start + n_samples > wav->length){
        // past the end of file
        return make_error(EWAVE_EOF, wav, "%s: tried reading beyond the end of the file (at sample number %llu)",
          wav->filename,
          (unsigned long long) sample_start);
    }

    fseek(wav->f, wav->pcm_start + sample_start * waveformat_get_sample_size(&wav->format), SEEK_SET);
    fread(buf, 1, n_samples * waveformat_get_sample_size(&wav->format), wav->f);
    return EOK;
}
