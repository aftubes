#ifndef ERRORS_H
#define ERRORS_H

typedef int err_t;

#define ERR_GROUP(number) ((number) << 12)

err_t make_error_internal(err_t n, void *obj, const char *file, const char *func, int line, const char *fmt, ...);
#define make_error(n, obj, ...) make_error_internal((n), (obj), __FILE__, __func__, __LINE__, __VA_ARGS__)

err_t get_last_error(void);
const char *get_last_error_file(void);
int get_last_error_line(void);
const char *get_last_error_func(void);
const char *get_last_error_message(void);

// some common error messages

enum {
    EOK = 0,
    ENOMEM,     // memory full!
    ENOTIMPL,   // not implemented
};

#endif
