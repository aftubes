#include "graph.h"
#include "errors.h"
#include "stdlib.h"
#include "string.h"
#include "modules/modules.h"
#include "assert.h"

static struct graphpin *get_last_pin(struct graphnode *node)
{
    struct graphpin *pin;
    for (pin=node->pins; pin && pin->next; pin=pin->next);
    return pin;
}

struct graphpin *graphnode_get_pin(struct graphnode *node, enum direction dir)
{
    struct graphpin *pin;
    for (pin=node->pins; pin; pin=pin->next){
        if (pin->dir == dir){
            break;
        }
    }
    return pin;
}

err_t graphnode_add_pin(struct graphnode *node, struct graphpin **pin_out)
{
    // TODO: allocate node + pins in one block?
    struct graphpin *pin = malloc(sizeof *pin), *pin_prev;
    if (!pin){
        // :-(
        return make_error(ENOMEM, node, "memory full: allocating graph node pin");
    }
    memset(pin, 0, sizeof *pin);
    pin->node = node;
    pin->edge = NULL;

    pin->next = NULL;
    pin_prev = get_last_pin(node);
    if (pin_prev){
        pin_prev->next = pin;
    } else {
        node->pins = pin;
    }

    *pin_out = pin;
    return EOK;
}

err_t graphnode_create(struct graphnode **node_out, const struct graphnode_functab *functab, size_t extra_bytes)
{
    struct graphnode *node;
    node = malloc(sizeof *node + extra_bytes);
    if (!node){
        return make_error(ENOMEM, NULL, "memory full: allocating graph node");
    }
    memset(node, 0, sizeof *node);
    node->functab = functab;
    node->extra = node->extra_data;
    *node_out = node;
    return EOK;
}

void graphnode_free(struct graphnode *node)
{
    struct graphpin *pin, *pin_next;
    struct graphedge *edge;

    pin = node->pins;
    while (pin){
        pin_next = pin->next;
        if (pin->edge){
            // it's connected
            // disconnect it (free the edge)
            edge = pin->edge;
            edge->a->edge = NULL;
            edge->b->edge = NULL;
            free(edge); // XXX: may need more freeing than this in the future
        }
        free(pin);
        pin = pin_next;
    }

    free(node);
}

bool graphnode_is_source(struct graphnode *node)
{
    struct graphpin *pin;
    for (pin=node->pins; pin; pin=pin->next){
        if (pin->dir != DIR_OUT){
            return false;
        }
    }
    return true;
}

bool graphnode_all_inputs_connected(struct graphnode *node)
{
    struct graphpin *pin;
    for (pin=node->pins; pin; pin=pin->next){
        if (pin->dir == DIR_IN && !pin->edge){
            return false;
        }
    }
    return true;
}

err_t graph_create(struct graph *graph)
{
    graph->node_first = graph->node_last = NULL;
    return EOK;
}

void graph_destroy(struct graph *graph)
{
    struct graphnode *node = graph->node_first, *node_next;
    while (node){
        node_next = node->next;
        graphnode_free(node);
        node = node_next;
    }
    graph->node_first = graph->node_last = NULL;
}

err_t graph_add_node(struct graph *graph, struct graphnode *node)
{
    node->prev = graph->node_last;
    node->next = NULL;
    *(graph->node_last ? &graph->node_last->next : &graph->node_first) = node;
    graph->node_last = node;
    return EOK;
}

err_t graph_remove_node(struct graph *graph, struct graphnode *node)
{
    *(node->prev ? &node->prev->next : &graph->node_first) = node->next;
    *(node->next ? &node->next->prev : &graph->node_last) = node->prev;
    return EOK;
}

// guess the format of output pin 'pin' - i.e. same as input format
static err_t guess_output_format(struct graphpin *pin, struct aformat *af)
{
    struct graphnode *node;
    struct graphpin *pin2;
    bool found_input = false;

    node = pin->node;
    for (pin2=node->pins; pin2; pin2=pin2->next){
        if (pin2->dir == DIR_IN){
            if (found_input){
                return make_error(ECANNOT_GUESS_FORMAT, node,
                  "cannot guess the output format of node \"%s\", because it has more than one input",
                  node->name);
            }
            found_input = true;
            if (!pin2->edge){
                // the input pin is not connected, so it won't have
                // a format, yet
                return make_error(ECANNOT_GUESS_FORMAT, node,
                  "cannot guess the output format of node \"%s\", because its input is not connected",
                  node->name);
            }
            *af = pin2->edge->buf.format;
        }
    }
    return EOK;
}

// create a converter node to convert between formats
static err_t create_converter_node(struct graph *graph, struct aformat *src_af, struct aformat *dest_af,
  struct graphpin *a, struct graphpin *b, struct graphnode **node_out)
{
    struct graphnode *node;
    struct graphpin *out_pin, *in_pin;
    err_t err;
    // create the new node
    err = audio_converter_create(&node, src_af, dest_af);
    if (err != EOK){
        return err;
    }

    err = graph_add_node(graph, node);
    if (err != EOK){
        return err;
    }

    // connect it
    out_pin = graphnode_get_pin(node, DIR_OUT);
    in_pin = graphnode_get_pin(node, DIR_IN);
    assert(out_pin);
    assert(in_pin);

    err = graph_connect(graph, a, in_pin);
    if (err != EOK){
        return err;
    }

    err = graph_connect(graph, out_pin, b);
    if (err != EOK){
        return err;
    }
    // done
    *node_out = node;
    return EOK;
}

static err_t do_connect(struct graph *graph, struct graphpin *a, struct graphpin *b, const struct aformat *af)
{
    struct graphedge *edge = malloc(sizeof *edge);
    err_t err;
    if (!edge){
        // ack!
        return make_error(ENOMEM, graph, "memory full: allocating edge");
    }
    
    buffer_init(&edge->buf);
    edge->buf.format = *af;

    if (a->node->functab->set_buffer){
        err = a->node->functab->set_buffer(a->node, a, &edge->buf);
        if (err != EOK){
            free(edge);
            return err;
        }
    }

    if (b->node->functab->set_buffer){
        err = b->node->functab->set_buffer(b->node, b, &edge->buf);
        if (err != EOK){
            free(edge);
            return err;
        }
    }
    
    edge->a = a;
    edge->b = b;
    edge->processed = false;
    a->edge = b->edge = edge;
    return EOK;
}

err_t graph_connect(struct graph *graph, struct graphpin *a, struct graphpin *b)
{
    struct aformat af, af2;
    struct graphnode *cvt_node;
    struct graphpin *cvt_input;
    err_t err;

    // check a and b direction
    if (!(a->dir == DIR_OUT && b->dir == DIR_IN)){
        // oh dear.
        return make_error(EPINDIR, graph,
          "incorrect pin directions while trying to connect");
    } else {
        // if a is a source node, it will be able to provide us with a media type
        // if a is not a source node, all its inputs must be connected
        // otherwise we won't know what output format to use.
        if (a->node->functab->get_output_format){
            err = a->node->functab->get_output_format(a->node, a, &af);
            if (err != EOK){
                return err;
            }
        } else {
            // assume output format is same as input format
            err = guess_output_format(a, &af);
            if (err != EOK){
                return err;
            }
        }
        // check media type with b node
        if (b->node->functab->is_acceptable_input_format
          && !b->node->functab->is_acceptable_input_format(b->node, b, &af)){
            // it doesn't like that input format
            // we'll have to add a converter node
            if (b->node->functab->get_ideal_input_format){
                err = b->node->functab->get_ideal_input_format(b->node, b, &af2);
                if (err != EOK){
                    return err;
                }
                cvt_input = NULL;
                return create_converter_node(graph, &af, &af2,
                  a, b, &cvt_node);
            } else {
                return make_error(ENO_AGREEABLE_FORMAT, graph, "cannot agree on a common media format (\"%s\" won't specify an ideal format)",
                  b->node->name);
            }
        }
        // actually do the connection
        return do_connect(graph, a, b, &af);
    }
}

// count the number of unprocessed incoming edges of a node
static int count_incoming(struct graphnode *node)
{
    struct graphpin *pin;
    int n = 0;
    for (pin=node->pins; pin; pin=pin->next){
        if (pin->dir == DIR_IN && pin->edge && !pin->edge->processed){
            ++n;
        }
    }
    return n;
}

err_t graph_sort(struct graph *graph)
{
    // stack of nodes
    struct stack_item {
        struct stack_item *prev;
        struct graphnode *node;
    } *S = NULL, *new, *old;

    struct graphnode *n, *m, *prev_L, **next_ptr_ptr;
    struct graphpin *pin;
    struct graphedge *edge;

    // find all source nodes
    for (n=graph->node_first; n; n=n->next){
        if (graphnode_is_source(n)){
            // insert n into S
            new = malloc(sizeof *new); // TODO: use alloca or something?
            if (!new){
                // uh oh
                // TODO: clean S
                return make_error(ENOMEM, NULL, "memory full: allocating stack item");
            }
            new->prev = S;
            new->node = n;
            S = new;
        }
    }

    // while S is not empty
    next_ptr_ptr = &graph->sorted_nodes;
    prev_L = NULL;
    while (S){
        // remove a node n from S
        n = S->node;
        old = S;
        S = S->prev;
        free(old);
        // insert n into L (the sorted list)
        n->sorted_prev = prev_L;
        *next_ptr_ptr = n;
        next_ptr_ptr = &n->sorted_next;
        prev_L = n;
        // for each node m with an edge from n to m
        for (pin=n->pins; pin; pin=pin->next){
            if (pin->dir != DIR_OUT
              || !pin->edge             // pin isn't connected (ignore it)
              || pin->edge->processed   // edge already 'removed' by algorithm
            ){
                continue;
            }
            edge = pin->edge;
            m = edge->b->node;
            // remove edge from graph (we actually want to keep it, so we just 
            // mark it, and not do any real removal)
            edge->processed = true;
            // does it have any other incoming edges?
            if (count_incoming(m) == 0){
                // no!
                // insert m into S
                new = malloc(sizeof *new);
                if (!new){
                    // ugh
                    // TODO: clean S
                    return make_error(ENOMEM, NULL, "memory full: allocating stack item");
                }
                new->prev = S;
                new->node = m;
                S = new;
            }
        }
    }
    // set the next pointer of the last node in the sequence to NULL
    // this finishes off the linked list.
    *next_ptr_ptr = NULL;

    // TODO: if the graph still has unprocessed edges, then the graph
    // has at least one cycle. Maybe we should do something about that.

    return EOK;
}

err_t graph_run(struct graph *graph)
{
    struct graphnode *node;
    err_t err;

    for (node=graph->sorted_nodes; node; node=node->sorted_next){
        if (node->functab->run){
            err = node->functab->run(node);
            if (err != EOK){
                return err;
            }
        }
    }
    return EOK;
}
