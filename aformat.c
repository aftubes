#include "aformat.h"

int aformat_get_sample_size(struct aformat *af)
{
    switch (af->media){
        case MT_AUDIO_32F:
            return sizeof(float) * af->channels;
        case MT_AUDIO_16I:
            return sizeof(short) * af->channels;
        case MT_AUDIO_32I:
            return sizeof(int) * af->channels;
        default:
            return 0;
    }
}

