#include "stdio.h"
#include "soundout.h"
#include "wavefile.h"
#include "modules/modules.h"
#include "graph.h"
#include "errors.h"
#include "assert.h"

#include "unistd.h"
#include "sys/types.h"
#include "signal.h"

void moan(err_t err)
{
    if (err != EOK){
        fprintf(stderr, "%s:%d: in function %s: %s\n",
          get_last_error_file(),
          get_last_error_line(),
          get_last_error_func(),
          get_last_error_message());
        raise(SIGABRT);
    }
}

int main(int argc, char **argv)
{
    struct graph _graph, *graph = &_graph;
    struct graphnode *node, *source_1, *source_2, *mixer, *sink;
    err_t err;

    if (graph_create(graph)){
        fprintf(stderr, "cannot create graph\n");
        return 1;
    }

    moan(wavesource_create(&node, "/home/aoe/reflections.wav"));
    assert(node);
    moan(graph_add_node(graph, node));
    node->name = "source_1";
    source_1 = node;

    moan(wavesource_create(&node, "/home/aoe/horizon.wav"));
    assert(node);
    moan(graph_add_node(graph, node));
    node->name = "source_2";
    source_2 = node;

    moan(playsink_create(&node));
    assert(node);
    moan(graph_add_node(graph, node));
    node->name = "sink";
    sink = node;

    moan(mixer_create(&node));
    assert(node);
    moan(graph_add_node(graph, node));
    node->name = "mixer";
    mixer = node;
    
    printf("connecting mixer in0\n");
    moan(graph_connect(graph, source_1->pins, mixer->pins->next));
    printf("connecting mixer in1\n");
    moan(graph_connect(graph, source_2->pins, mixer->pins->next->next));
    printf("connecting mixer out\n");
    moan(graph_connect(graph, mixer->pins, sink->pins));
    moan(graph_sort(graph));

    {
        struct graphnode *node;
        for (node=graph->sorted_nodes; node; node=node->sorted_next){
            printf("sorted_node: %s\n", node->name);
        }
    }

    while ((err = graph_run(graph)) == EOK);
    moan(err);

    graph_destroy(graph);


    return 0;
}
