#ifndef GRAPH_H
#define GRAPH_H

#include "stdbool.h"
#include "buffer.h"
#include "errors.h"

enum {
    EPINDIR = ERR_GROUP(7),     // incorrect pin directions
    EUNCONNECTED,               // there are unconnected pins
    ENO_AGREEABLE_FORMAT,       // no format that both nodes will agree on
    ECANNOT_GUESS_FORMAT,       // cannot guess a node's output format
    EBAD_FORMAT,                // bad format
};

enum direction {
    DIR_IN,
    DIR_OUT,
};

struct graphnode;
struct graphpin;
struct graphedge;

struct graphnode_functab {
    bool (* is_acceptable_input_format)(struct graphnode *node, struct graphpin *pin, const struct aformat *af);
    err_t (* get_ideal_input_format)(struct graphnode *node, struct graphpin *pin, struct aformat *af);
    err_t (* get_output_format)(struct graphnode *node, struct graphpin *pin, struct aformat *af);
    err_t (* set_buffer)(struct graphnode *node, struct graphpin *pin, struct buffer *buf);
    err_t (* run)(struct graphnode *node);
};

struct graphedge {
    struct graphpin *a, *b;
    struct buffer buf;
    bool processed;
};

struct graphpin {
    struct graphpin *next;
    struct graphnode *node;
    struct graphedge *edge;
    const char *name;
    enum direction dir;
    int tag;                    // user-defined value
};

struct graphnode {
    const struct graphnode_functab *functab;
    struct graphnode *prev, *next;          // nodes in a list (doesn't relate to data flow)
    struct graphnode *sorted_prev,
                     *sorted_next;  // nodes in topological order (the order to run them in)
    struct graphpin *pins;
    const char *name;               // name (for debugging, atm)
    void *extra;                    // pointer to additional data, if needed
    char extra_data[];
};

struct graph {
    struct graphnode *node_first, *node_last,
                     *sorted_nodes;
};

struct graphpin *graphnode_get_pin(struct graphnode *node, enum direction dir); // return the first pin of direction 'dir'
err_t graphnode_add_pin(struct graphnode *node, struct graphpin **pin_out); // create a new pin for the node
void graphnode_remove_pin(struct graphnode *node, struct graphpin *pin);    // remove a pin from this node

err_t graphnode_create(struct graphnode **node_out, const struct graphnode_functab *functab, size_t extra_bytes);         // create new node (with extra_bytes additional space)
void graphnode_free(struct graphnode *node);        // destroy a node
bool graphnode_is_source(struct graphnode *node);   // returns true if node is a source (no inputs)
bool graphnode_all_inputs_connected(struct graphnode *node);    // returns true if ... well, have a guess!

err_t graph_create(struct graph *graph);            // create a new graph
void graph_destroy(struct graph *graph);            // destroy a graph and its contents
err_t graph_add_node(struct graph *graph, struct graphnode *node);      // add a node to the graph (doesn't connect anything - just adds it)
err_t graph_remove_node(struct graph *graph, struct graphnode *node);   // removes a node from the graph (doesn't destroy the node)

// Make connections. Pins a and b must be unconnected.
// Pin a must be an output, pin b must be an input.
err_t graph_connect(struct graph *graph, struct graphpin *a, struct graphpin *b);

err_t graph_sort(struct graph *graph);                // does a topological sort of graph nodes (must be called before running the graph, after the graph has changed)
err_t graph_run(struct graph *graph);                 // run the graph! (actually does stuff)

#endif
