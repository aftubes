#include "graph.h"
#include "wavefile.h"

struct wavefile_source {
    struct buffer *buf;
    struct wavefile wf;
    off_t pos;
};

static err_t get_output_format(struct graphnode *node, struct graphpin *pin,
  struct aformat *af)
{
    struct wavefile_source *ws = node->extra;
    af->media = MT_AUDIO_16I;
    af->srate = ws->wf.format.srate;
    af->channels = ws->wf.format.channels;
    return EOK;
}

static err_t set_buffer(struct graphnode *node, struct graphpin *pin, struct buffer *buf)
{
    struct wavefile_source *ws = node->extra;
    ws->buf = buf;
    return EOK;
}

static err_t run(struct graphnode *node)
{
    struct wavefile_source *ws = node->extra;
    err_t err;

    err = buffer_alloc(ws->buf, 4410);
    if (err != EOK){
        return err;
    }

    err = wavefile_read_at(&ws->wf, ws->pos, ws->buf->data, 4410);
    if (err != EOK){
        return err;
    }

    ws->pos += 4410;

    return EOK;
}

static const struct graphnode_functab functab = {
    NULL,   // is_acceptable_input_format
    NULL,   // get_ideal_input_format
    get_output_format,
    set_buffer,
    run,
};

err_t wavesource_create(struct graphnode **node_out, const char *filename)
{
    struct graphnode *node;
    struct graphpin *pin;
    struct wavefile_source *ws;
    err_t err;

    err = graphnode_create(&node, &functab, sizeof *ws);
    if (err != EOK){
        return err;
    }

    err = graphnode_add_pin(node, &pin);
    if (err != EOK){
        return err;
    }
    pin->dir = DIR_OUT;
    pin->name = "out";

    ws = node->extra;
    err = wavefile_open(&ws->wf, filename);
    if (err != EOK){
        graphnode_free(node);
        return err;
    }
    ws->pos = 0;

    *node_out = node;
    return EOK;
}

