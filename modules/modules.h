#ifndef MODULES_H
#define MODULES_H

#include "graph.h"
#include "errors.h"

err_t wavesource_create(struct graphnode **node_out, const char *filename);
err_t playsink_create(struct graphnode **node_out);
err_t ringmod_create(struct graphnode **node_out);
err_t audio_converter_create(struct graphnode **node_out, struct aformat *src_af, struct aformat *dest_af);
err_t mixer_create(struct graphnode **node_out);

#endif
