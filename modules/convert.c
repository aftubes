#include "modules.h"
#include "graph.h"
#include "assert.h"

struct converter {
    struct graphpin *in_pin, *out_pin;
    struct aformat src_af, dest_af;
};

static err_t get_output_format(struct graphnode *node, struct graphpin *pin, struct aformat *af)
{
    struct converter *c = node->extra;
    assert(pin == c->out_pin);
    *af = c->dest_af;
    return EOK;
}

static err_t run(struct graphnode *node)
{
#define DO_CONVERT(s_T, d_T, SCALE) { \
    s_T *restrict s_ptr = s_buf->data; \
    d_T *restrict d_ptr = d_buf->data; \
    int i; \
    for (i=s_buf->n_samples * s_buf->format.channels; i; --i){ \
        (*(d_ptr++)) = (d_T) ((*(s_ptr++)) SCALE); \
    } \
    return EOK; }

    struct converter *c = node->extra;
    struct buffer *s_buf = &c->in_pin->edge->buf,
                  *d_buf = &c->out_pin->edge->buf;
    err_t err;

    err = buffer_alloc(d_buf, s_buf->n_samples);
    if (err != EOK){
        return err;
    }

    switch (c->src_af.media){
        case MT_AUDIO_32F:
            switch (c->dest_af.media){
                case MT_AUDIO_16I:
                    DO_CONVERT(float, short, * 32768.0)
                case MT_AUDIO_32I:
                    DO_CONVERT(float, int, * 2147483648.0)
            }
        case MT_AUDIO_16I:
            switch (c->dest_af.media){
                case MT_AUDIO_32F:
                    DO_CONVERT(short, float, / 32768.0)
                case MT_AUDIO_32I:
                    DO_CONVERT(short, int, >> 16)
            }
        case MT_AUDIO_32I:
            switch (c->dest_af.media){
                case MT_AUDIO_32F:
                    DO_CONVERT(int, float, / 2147483648.0)
                case MT_AUDIO_16I:
                    DO_CONVERT(int, short, << 16)
            }
    }
    return make_error(ENOTIMPL, NULL, "conversion between these media types is not implemented");
}

static const struct graphnode_functab functab = {
    NULL,   // is_acceptable_input_format
    NULL,   // get_ideal_input_format
    get_output_format,
    NULL,   // set_buffer
    run,
};

err_t audio_converter_create(struct graphnode **node_out, struct aformat *src_af, struct aformat *dest_af)
{
    struct graphnode *node;
    struct converter *restrict c;
    err_t err;

    err = graphnode_create(&node, &functab, sizeof *c);
    if (err != EOK){
        return err;
    }
    c = node->extra;

    c->src_af = *src_af;
    c->dest_af = *dest_af;

    err = graphnode_add_pin(node, &c->in_pin);
    if (err != EOK){
        return err;
    }
    c->in_pin->dir = DIR_IN;
    c->in_pin->name = "in";

    err = graphnode_add_pin(node, &c->out_pin);
    if (err != EOK){
        return err;
    }
    c->out_pin->dir = DIR_OUT;
    c->out_pin->name = "out";

    *node_out = node;
    return EOK;
}
