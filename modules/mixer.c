#include "modules.h"
#include "graph.h"
#include "errors.h"
#include "assert.h"
#include "stdio.h"

///// Simple additive mixer /////
///// TODO: handle different input formats /////

struct mixer {
    bool format_is_set;
    struct aformat af;
    struct graphpin *out_pin;
    int n_inputs;
};

static bool is_acceptable_input_format(struct graphnode *node, struct graphpin *pin, const struct aformat *af)
{
    struct mixer *m = node->extra;
    assert(pin != m->out_pin);
    if (m->format_is_set){
        return af->media == m->af.media
            && af->srate == m->af.srate
            && af->channels == m->af.channels;
    } else {
        return true;
    }
}

static err_t get_output_format(struct graphnode *node, struct graphpin *pin, struct aformat *af)
{
    struct mixer *m = node->extra;
    assert(pin == m->out_pin);

    if (m->format_is_set){
        *af = m->af;
        return EOK;
    } else {
        return make_error(ECANNOT_GUESS_FORMAT, node, "mixer module is not yet bound to a format");
    }
}

static err_t another_in_pin(struct graphnode *node, struct mixer *m)
{
    // create a new input pin
    struct graphpin *new_pin;
    err_t err;
    char *new_name = NULL;

    err = graphnode_add_pin(node, &new_pin);
    if (err != EOK){
        return err;
    }
    asprintf(&new_name, "in%d", m->n_inputs);
    new_pin->name = new_name;
    new_pin->dir = DIR_IN;
    ++m->n_inputs;
    return EOK;
}

static err_t set_buffer(struct graphnode *node, struct graphpin *pin, struct buffer *buf)
{
    struct mixer *m = node->extra;
    if (m->format_is_set){
        if (buf->format.media == m->af.media
          && buf->format.srate == m->af.srate
          && buf->format.channels == m->af.channels){
            return another_in_pin(node, m);
        } else {
            return make_error(EBAD_FORMAT, node, "mixer module is being connected to a bad media format");
        }
    } else {
        m->af = buf->format;
        m->format_is_set = true;
        return another_in_pin(node, m);
    }
}

static size_t longest_in_buf_len(struct graphnode *restrict node)
{
    struct graphpin *in_pin;
    size_t sz = 0;

    for (in_pin=node->pins; in_pin; in_pin=in_pin->next){
        if (in_pin->dir != DIR_IN || !in_pin->edge){
            continue;
        }
        if (in_pin->edge->buf.n_samples > sz){
            sz = in_pin->edge->buf.n_samples;
        }
    }
    return sz;
}

static err_t run(struct graphnode *node)
{
#define DO_MIX(T) { \
    T *restrict s_ptr, *restrict d_ptr; \
    register int i; \
    /* clear the output buffer */ \
    d_ptr = d_buf->data; \
    for (i=sz; i; --i){ \
        *d_ptr++ = 0; \
    } \
    \
    for (in_pin=node->pins; in_pin; in_pin=in_pin->next){ \
        if (in_pin->dir != DIR_IN || !in_pin->edge){ \
            continue; \
        } \
        s_ptr = in_pin->edge->buf.data; \
        d_ptr = d_buf->data; \
        for (i=sz; i; --i){ \
            /* Mix! */ \
            *d_ptr++ += *s_ptr++ / m->n_inputs; /* *boggle* */ \
        } \
    } \
    return EOK; }

    struct mixer *restrict m = node->extra;
    struct graphpin *in_pin;
    struct buffer *restrict d_buf = &m->out_pin->edge->buf;
    size_t sz;
    err_t err;

    sz = longest_in_buf_len(node);
    err = buffer_alloc(d_buf, sz);
    if (err != EOK){
        return err;
    }

    sz *= m->out_pin->edge->buf.format.channels;

    switch (m->out_pin->edge->buf.format.media){
        case MT_AUDIO_32F:
            DO_MIX(float)
        case MT_AUDIO_16I:
            DO_MIX(short)
        case MT_AUDIO_32I:
            DO_MIX(int)
    }
    return make_error(ENOTIMPL, NULL, "mixing these media types is not implemented");
}

static const struct graphnode_functab functab = {
    is_acceptable_input_format,
    NULL,   // get_ideal_input_format
    get_output_format,
    set_buffer,
    run,
};

err_t mixer_create(struct graphnode **node_out)
{
    struct graphnode *node;
    struct mixer *m;
    struct graphpin *in_pin;
    err_t err;

    err = graphnode_create(&node, &functab, sizeof *m);
    if (err != EOK){
        return err;
    }
    m = node->extra;

    m->n_inputs = 1;
    m->format_is_set = false;

    // create output pin
    err = graphnode_add_pin(node, &m->out_pin);
    if (err != EOK){
        return err;
    }
    m->out_pin->dir = DIR_OUT;
    m->out_pin->name = "out";

    // create input pins
    err = graphnode_add_pin(node, &in_pin);
    if (err != EOK){
        return err;
    }
    in_pin->dir = DIR_IN;
    in_pin->name = "in0"; // how original

    *node_out = node;
    return EOK;
}
