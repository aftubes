#include "modules.h"
#include "graph.h"
#include "errors.h"
#include "math.h"

struct ringmod {
    struct graphpin *in_pin, *out_pin;
    float pos;
};

static bool is_acceptable_input_format(struct graphnode *node, struct graphpin *pin, const struct aformat *af)
{
    return af->media == MT_AUDIO_32F;
}

static err_t get_ideal_input_format(struct graphnode *node, struct graphpin *pin, struct aformat *af)
{
    af->media = MT_AUDIO_32F;
    af->srate = 44100;
    af->channels = 2;
    return EOK;
}

static err_t run(struct graphnode *node)
{
    struct ringmod *rm = node->extra;
    float *src, *dest, pos;
    err_t err;
    int i;
    
    err = buffer_alloc(&rm->out_pin->edge->buf, rm->in_pin->edge->buf.n_samples);
    if (err != EOK){
        return err;
    }

    src = rm->in_pin->edge->buf.data;
    dest = rm->out_pin->edge->buf.data;
    pos = rm->pos;

    for (i = rm->in_pin->edge->buf.n_samples * rm->in_pin->edge->buf.format.channels;
      i; --i){
        *dest = *src * cos((pos * M_PI * 2) / 441.0);
        ++dest, ++src, ++pos;
    }

    rm->pos = pos;

    return EOK;
}

static const struct graphnode_functab functab = {
    is_acceptable_input_format,
    get_ideal_input_format,
    NULL,       // set_output_format
    NULL,       // set_buffer
    run,
};

err_t ringmod_create(struct graphnode **node_out)
{
    struct graphnode *node;
    struct ringmod *restrict rm;
    err_t err;

    err = graphnode_create(&node, &functab, sizeof *rm);
    if (err != EOK){
        return err;
    }

    rm = node->extra;
    rm->pos = 0.0;

    err = graphnode_add_pin(node, &rm->in_pin);
    if (err != EOK){
        graphnode_free(node);
        return err;
    }
    rm->in_pin->dir = DIR_IN;
    rm->in_pin->name = "in";

    err = graphnode_add_pin(node, &rm->out_pin);
    if (err != EOK){
        graphnode_free(node);
        return err;
    }
    rm->out_pin->dir = DIR_OUT;
    rm->out_pin->name = "out";
    
    *node_out = node;
    return EOK;
}
