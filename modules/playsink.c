#include "graph.h"
#include "errors.h"
#include "soundout.h"

struct playsink {
    struct buffer *buf;
    bool is_open;
    struct soundout so;
};

static bool is_acceptable_input_format(struct graphnode *node, struct graphpin *pin, const struct aformat *af)
{
    return af->media == MT_AUDIO_16I;
}

static err_t get_ideal_input_format(struct graphnode *node, struct graphpin *pin, struct aformat *af)
{
    af->media = MT_AUDIO_16I;
    af->srate = 44100;
    af->channels = 2;
    return EOK;
}

static err_t set_buffer(struct graphnode *node, struct graphpin *pin, struct buffer *buf)
{
    struct playsink *ps = node->extra;
    ps->buf = buf;
    if (!ps->is_open){
        if (soundout_open(&ps->so, &buf->format) != 0){
            return make_error(-1, node, "cannot open soundout");
        }
        ps->is_open = true;
    }
    return EOK;
}

static err_t run(struct graphnode *node)
{
    struct playsink *ps = node->extra;
    
    if (soundout_write(&ps->so, ps->buf->data, ps->buf->n_samples * aformat_get_sample_size(&ps->buf->format)) != 0){
        return make_error(-1, node, "cannot write to soundout");
    }
    return EOK;
}

static const struct graphnode_functab functab = {
    is_acceptable_input_format,
    get_ideal_input_format,
    NULL,   // get_output_format
    set_buffer,
    run,
};

err_t playsink_create(struct graphnode **node_out)
{
    struct graphnode *node;
    struct graphpin *pin;
    struct playsink *ps;
    err_t err;

    err = graphnode_create(&node, &functab, sizeof *ps);
    if (err != EOK){
        return err;
    }

    err = graphnode_add_pin(node, &pin);
    if (err != EOK){
        return err;
    }
    pin->dir = DIR_IN;
    pin->name = "in";

    ps = node->extra;
    ps->is_open = false;

    *node_out = node;
    return EOK;
}
