#include "errors.h"
#include "stdarg.h"
#include "stdio.h"

static __thread struct {
    err_t errno;
    const char *file;
    const char *func;
    int line;
    char *msg;
    void *object;
} errinfo = {0, NULL, NULL, 0, NULL, NULL};

err_t make_error_internal(err_t n, void *obj, const char *file, const char *func, int line, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vasprintf(&errinfo.msg, fmt, ap);
    va_end(ap);
    errinfo.errno = n;
    errinfo.file = file;
    errinfo.func = func;
    errinfo.line = line;
    errinfo.object = obj;
    return n;
}

err_t get_last_error(void)
{
    return errinfo.errno;
}

const char *get_last_error_file(void)
{
    return errinfo.file;
}

const char *get_last_error_func(void)
{
    return errinfo.func;
}

int get_last_error_line(void)
{
    return errinfo.line;
}

const char *get_last_error_message(void)
{
    return errinfo.msg;
}
