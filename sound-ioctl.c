/* Easier writing these bits in C rather
   than defining the ioctls in D */

#include "fcntl.h"
#include "sys/ioctl.h"
#include "linux/soundcard.h"

int sound_set_format(int fd, int format)
{
    int tmp;
    tmp = format;
    if (ioctl(fd, SNDCTL_DSP_SETFMT, &tmp) == -1
      || tmp != format)
    {
        return -1;
    } else {
        return 0;
    }
}

int sound_set_srate(int fd, int srate)
{
    int tmp;
    tmp = srate;
    if (ioctl(fd, SNDCTL_DSP_SPEED, &tmp) == -1
      || tmp != srate)
    {
        return -1;
    } else {
        return 0;
    }
}

int sound_set_channels(int fd, int channels)
{
    int tmp;
    tmp = channels;
    if (ioctl(fd, SNDCTL_DSP_CHANNELS, &tmp) == -1
      || tmp != channels)
    {
        return -1;
    } else {
        return 0;
    }
}

