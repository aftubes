#ifndef AFORMAT_H
#define AFORMAT_H

// media types
enum {
    MT_UNKNOWN = 0,
    MT_AUDIO_32F,   // 32-bit floating-point audio
    MT_AUDIO_16I,   // 16-bit signed integer
    MT_AUDIO_32I,   // 32-bit signed integer
};

struct aformat {
    int media;      // media type (MT_*)
    int srate;      // sample rate
    int channels;   // number of channels
};

int aformat_get_sample_size(struct aformat *af);

#endif
