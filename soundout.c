#include "sound-ioctl.h"
#include "soundout.h"
#include "unistd.h"
#include "fcntl.h"

int soundout_open(struct soundout *so, const struct aformat *af)
{
    so->fd = open("/dev/dsp", O_WRONLY);
    if (so->fd < 0){
        return 1;
    }

    if (sound_set_format(so->fd, 16)){
        return 1;
    }

    if (sound_set_srate(so->fd, af->srate)){
        return 1;
    }

    if (sound_set_channels(so->fd, af->channels)){
        return 1;
    }

    return 0;
}

int soundout_write(struct soundout *so, const void *buf, size_t len)
{
    write(so->fd, buf, len);
    return 0;
}
