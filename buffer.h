#ifndef BUFFER_H
#define BUFFER_H

#include "aformat.h"
#include "errors.h"
#include "stddef.h"

struct buffer {
    struct aformat format;
    size_t n_samples;
    void *data;
};

void buffer_init(struct buffer *buf);
err_t buffer_alloc(struct buffer *buf, size_t n_samples);

#endif
